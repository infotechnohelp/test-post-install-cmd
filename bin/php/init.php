<?php

$renderscriptDirPath = dirname(__DIR__, 5) . '/renderscript';

@mkdir($renderscriptDirPath);

copy(dirname(__DIR__, 2) . '/dist/config.php', $renderscriptDirPath . '/config.php');


// --demo

$gitignorePath = dirname(__DIR__, 5) . '/.gitignore';

$gitignoreString = '/renderscript/config.php';

if (file_exists($gitignorePath)) {

    $gitignoreContents = file_get_contents($gitignorePath);

    if (strpos($gitignoreContents, $gitignoreString) === false){
        file_put_contents($gitignorePath, "$gitignoreString\n$gitignoreContents");
    }

    exit;
}

file_put_contents($gitignorePath, $gitignoreString);



// vendor/bin/renderscript test-connection
