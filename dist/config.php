<?php return [
    'url' => 'https://api.renderscript.com',
    'username' => 'username',
    'password' => 'password',
    'tmpTokenPath' => __DIR__ . '/tmp/token',
    'outputPath' => dirname(__DIR__) . '/',
];
